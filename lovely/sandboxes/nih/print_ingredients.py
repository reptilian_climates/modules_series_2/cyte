
def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../structures',
	'../structures_pip'
])


from BOTANY.IMPORT import IMPORT
api_key = IMPORT ("/ONLINE_KEYS/NIH/__init__.py").keys () ["API"]


chia_seeds_214893 = 214893;


import cyte.supplements.NIH.API.one as NIH_API_one
NIH_supplement = NIH_API_one.find (
	chia_seeds_214893,
	api_key
)



NIH_supplement_data = NIH_supplement ["data"]

# import pyjsonviewer
# pyjsonviewer.view_data (json_data = NIH_supplement_data)

import cyte.structs.DB.access as access
structs_db = access.DB ()

import cyte.supplements.NIH.struct_2.ingredients.quantified.writer as quantified_ingredient_writer
quantified_ingredient_writer.write (
	NIH_supplement_data,
	structs_db = structs_db
)
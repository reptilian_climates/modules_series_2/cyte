



def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])


import cyte._ovals as ovals
USDA_api_key = ovals.find () ['USDA'] ['food']

import cyte.hygiene.__stem.climate as stem_climate
stem_climate.change ()

import cyte.hygiene._system.connect as connect
[ r, c ] = connect.now ()


import cyte.hygiene._aggregates.recipes as aggregate_recipe
'''
recipe = aggregate_recipe.calc (
	foods = [ '2449699', '2138281', '2548008' ],
	supplements = [ '2261967', '214893' ]
)
'''

recipe = aggregate_recipe.calc (
	foods = [],
	supplements = [ '248267' ]
)

recipe_struct_grove = recipe.recipe_struct_grove

import json
#print (json.dumps (recipe_struct_grove, indent = 4))


def write (structs, level = 0):
	indent = " " * (level * 4)
	structs.sort (key = lambda struct : struct ["names"][0])

	for struct in structs:
		print (f"{ indent }{ struct['names'] } { struct['region'] }")
		print (f"	{ indent }sources: { len (struct['ingredients']) }")
		print (f"	{ indent }mass in grams: { struct['mass']['per package']['float string grams'] }")

		print ()

		if ("includes structs" in struct):
			write (struct ["includes structs"], level = (level + 1))

	
#write (recipe_struct_grove)

#import pyjsonviewer
#pyjsonviewer.view_data(json_data=recipe_struct_grove)


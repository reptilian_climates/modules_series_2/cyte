
def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])


import cyte._ovals as ovals
USDA_api_key = ovals.find () ['USDA'] ['food']

import cyte.hygiene.__stem.climate as stem_climate
stem_climate.change ()

import cyte.hygiene.foods.inventory.doc.insert as insert_food
returns = insert_food.now ({
	"fdc_id": 2548008,
	"api_key": USDA_api_key,
	
	"insert": "yes"
})

document = returns ["document"]

'''
import cyte.hygiene.foods.inventory.doc_examples.add as add_food
add_food.hastily (
	name = "truffle_1960255.json",
	document = document
)
'''

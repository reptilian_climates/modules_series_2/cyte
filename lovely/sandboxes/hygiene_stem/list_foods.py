


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])


import cyte._ovals as ovals
USDA_api_key = ovals.find () ['USDA'] ['food']

import cyte.hygiene.__stem.climate as stem_climate
stem_climate.change ()

import cyte.hygiene._system.connect as connect
[ r, c ] = connect.now ()

import cyte.hygiene.foods.inventory.doc.list.fdc_ids as fdc_id_list
fdc_ids = fdc_id_list.find ()

print (fdc_ids)




def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../structures',
	'../../../structures_pip'
])


import cyte._ovals as ovals
USDA_api_key = ovals.find () ['USDA'] ['food']

import cyte.hygiene.__stem.climate as stem_climate
stem_climate.change ()

import cyte.hygiene._system.connect as connect
[ r, c ] = connect.now ()

'''
emblem = 4
import cyte.hygiene.foods.inventory.doc.augment_food_struct_2 as augment_food_struct_2
augment_food_struct_2.now (emblem)
'''

import cyte.hygiene.foods.inventory.doc.augment_every_food_struct_2 as augment_every_food_struct_2
augment_every_food_struct_2.now ()
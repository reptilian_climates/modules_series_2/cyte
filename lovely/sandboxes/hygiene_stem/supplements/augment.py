



def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../../structures',
	'../../../structures_pip'
])



import cyte.hygiene.__stem.climate as stem_climate
stem_climate.change ()

import cyte.hygiene._system.connect as connect
[ r, c ] = connect.now ()

emblem = 1

import cyte.hygiene.supplements.inventory.doc.augment_struct_2 as augment_supplement_struct_2
augment_supplement_struct_2.now (1)
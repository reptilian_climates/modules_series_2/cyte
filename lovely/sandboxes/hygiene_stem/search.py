






def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])


import cyte.hygiene.__stem.climate as stem_climate
stem_climate.change ()


import cyte.hygiene._aggregates.search as search
returns = search.start ({
	"product name": ""
})

products = returns ["products"]
for product in products:
	print (product ["struct_2"]["product"]["name"])

print ("returns:", len (returns ["products"]))
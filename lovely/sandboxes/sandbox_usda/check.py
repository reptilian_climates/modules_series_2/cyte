
import json

def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../structures',
	'../structures_pip'
])


fdc_id = 1960255


import cyte._ovals as ovals
API_USDA_ellipse = ovals.find () ['USDA'] ['food']

import cyte.food.USDA.API.one as USDA_food_API
food = USDA_food_API.find (
	fdc_id,
	API_ellipse = API_USDA_ellipse,
	kind = "branded"
)

USDA_food_data = food ["data"]
USDA_food_source = food ["source"]

import cyte.food.USDA.struct_2 as USDA_struct_2
food_struct_2 = USDA_struct_2.calc (USDA_food_data)

print (json.dumps (food_struct_2, indent = 4))


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

from tinydb import TinyDB, Query


remove_region = 13

import cyte.structs.DB.access as access
struct_db = access.DB ()
removal = struct_db.remove (Query ().region == remove_region)

print ("removal:", removal)


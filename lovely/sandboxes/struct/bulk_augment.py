


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../structures',
	'../structures_pip'
])


from tinydb import TinyDB, Query
import json

import cyte.structs.DB.access as access
struct_db = access.DB ()


def for_each (struct):
	print ("struct:", struct)
	
	if ("PART OF" in struct):
		from tinydb.operations import delete
		struct_db.update (
			delete ('PART OF'), 
			Query ().region == struct['region']
		)



import cyte.structs.scan as struct_scan
structs = struct_scan.START (
	struct_db,
	FOR_EACH = for_each
)
#
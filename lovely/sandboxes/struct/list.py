




def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

import json
from tinydb import TinyDB, Query

import cyte.structs.DB.access as access
import cyte.structs.scan.trees_form_1.printer as trees_form_1_printer
import cyte.structs.scan.trees_form_1 as trees_form_1

struct_db = access.DB ()
trees_form_1_grove = trees_form_1.start (struct_db)
trees_form_1_printer.write (trees_form_1_grove)
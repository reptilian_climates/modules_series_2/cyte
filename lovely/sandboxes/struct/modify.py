
def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

from tinydb import TinyDB, Query

import json
import cyte.structs.DB.access as access
import cyte.structs.scan.names.has as struct_has_name

struct_db = access.DB ()


find = 'vitamin d'
replace = [ 
	'vitamin d', 'vitamin d (d2 + d3)', 'vitamin d2', 'vitamin d3',
	'vitamin d (d2 + d3), international units' 	
]



struct = struct_has_name.search (
	struct_db,
	name = find
)

updated = struct_db.update ({ 
	'names': replace 
}, Query ().region == struct ["region"])



print ("updated:", updated)


#copy.deepcopy(
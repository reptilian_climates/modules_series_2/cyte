


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../structures',
	'../structures_pip'
])


import cyte.structs.DB.access as access
import cyte.structs.scan.names.has as struct_has_name
struct = struct_has_name.search (
	access.DB (),
	name = "protein"
)



#
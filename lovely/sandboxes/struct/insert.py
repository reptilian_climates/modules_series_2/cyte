

'''
	It is better to modify the structs,
	for the sake of status checks.
'''

def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])

to_insert = {
	"names": [ "iron", "iron, fe" ],
	"includes": []
}

import cyte.structs.DB.access as access
struct_db = access.DB ()

import cyte.structs.sculpt.struct.insert as struct_insert
returns = struct_insert.START (
	struct_db,
	to_insert
)

print ("inserted:", returns)


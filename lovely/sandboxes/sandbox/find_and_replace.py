





import glob
import os.path

def START (
	GLOB = "",
	
	FIND = "",
	REPLACE_WITH = ""
):
	FILES = glob.glob (GLOB, recursive = True)

	for FILE in FILES:
		IS_FILE = os.path.isfile (FILE) 
	
		if (IS_FILE == True):
			print (FILE)

	for FILE in FILES:
		IS_FILE = os.path.isfile (FILE) 
	
		if (IS_FILE == True):		
			#print ("checking:", FILE)
		
			try:
				with open (FILE, "r") as FP_1:
					ORIGINAL = FP_1.read ()
					NEW_STRING = ORIGINAL.replace (FIND, REPLACE_WITH)
			
				if (ORIGINAL != NEW_STRING):
					print ("replacing:", FILE)
					#print ("NEW_STRING:", NEW_STRING)
					
					
					with open (FILE, "w") as FP_2:
						FP_2.write (NEW_STRING)
			
			except Exception as E:
				print ("exception:")
				print (" ", FILE)
				print (" ", E)
				print ()
				


import pathlib
from os.path import dirname, join, normpath
this_folder = pathlib.Path (__file__).parent.resolve ()
	
START (
	GLOB = normpath (join (this_folder, "../structures/cyte")) + "/**/*",
	#GLOB = str (this_folder) + "/DB/**/*",
	FIND = 'NAMES',
	REPLACE_WITH = 'names'
)
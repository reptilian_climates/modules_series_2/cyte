




def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])


import cyte.food.USDA.examples as USDA_examples
import cyte.food.USDA.struct_2 as USDA_struct_2

import cyte.supplements.NIH.examples as NIH_examples
import cyte.supplements.NIH.struct_2 as struct_2

import cyte.recipes.s2_tf1 as struct_2_recipes

import cyte.structs.scan.trees_form_1.find.region as find_region

walnuts_1882785 = USDA_struct_2.calc (
	USDA_examples.RETRIEVE ("branded/walnuts_1882785.json")
)	
calcium_261967 = struct_2.calc (
	NIH_examples.RETRIEVE ("tablets/calcium_261967.JSON")
)
chia_seeds_214893 = struct_2.calc (NIH_examples.RETRIEVE ("other/chia_seeds_214893.json"))

recipe = struct_2_recipes.calc ({
	"products": [
		chia_seeds_214893,
		walnuts_1882785
	]
})

recipe_struct_grove = recipe.recipe_struct_grove;

import cyte.recipes.s2_tf1.print as print_struct_2_recipes
print_struct_2_recipes.currently (recipe_struct_grove)

import pyjsonviewer
pyjsonviewer.view_data (json_data = recipe_struct_grove)






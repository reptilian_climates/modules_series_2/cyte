
'''
	python3 status/statuses/vitals/__init__.py "food_and_supps/struct_2/ingredients/quantified_grove/STATUS_find_ingredient_1.py"
'''

import cyte.food.USDA.examples as USDA_examples

import cyte.food.USDA.struct_2 as USDA_struct_2
import cyte.food.USDA.struct_2.ingredients.quantified_grove.printer as quantified_grove_printer
import cyte.food_and_supps.struct_2.ingredients.quantified_grove.find_ingredient as find_ingredient

import json

def check_1 ():
	food_struct_2 = USDA_struct_2.calc (
		USDA_examples.retrieve ("branded/walnuts_1882785.json")
	)
	ingredient = find_ingredient.calc (
		food_struct_2,
		"protein"
	)
	assert (ingredient ['name'] == "Protein")

	print (json.dumps (ingredient, indent = 4))

	ingredient = find_ingredient.calc (
		food_struct_2,
		"vitamin d"
	)
	assert (ingredient ['name'] == "Vitamin D (D2 + D3), International Units")


	return;
	
	
checks = {
	'check 1': check_1
}
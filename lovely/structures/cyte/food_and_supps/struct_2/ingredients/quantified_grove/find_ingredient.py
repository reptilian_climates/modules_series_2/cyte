
'''
import cyte.food_and_supps.struct_2.ingredients.quantified_grove.find_ingredient as find_ingredient
ingredient = find_ingredient.calc (
	treasure_struct_2,
	ingredient_name
)
'''

def find (
	quantified_grove,
	ingredient_name
):
	for ingredient in quantified_grove:
	
		struct_names = ingredient ["struct"] ["names"]
		
		for name in struct_names:
			if (name == ingredient_name):
				return ingredient;
		
		ingredient_quantified_grove = ingredient ["quantified grove"]
		found = find (ingredient_quantified_grove, ingredient_name)
		if (type (found) == dict):
			return found

	return False

def calc (
	treasure_struct_2,
	ingredient_name = "",
	include_effectual = True
):
	#
	#	trees_form_1_grove
	#
	quantified_grove = treasure_struct_2 ["ingredients"] ["quantified grove"]

	ingredient = find (
		quantified_grove,
		ingredient_name
	)


	return ingredient

	
	
	
	
	
	
	
	
	
	
	
	
	
	

'''
	python3 status/statuses/vitals/__init__.py "food_and_supps/struct_2/ingredients/quantified_grove/mass_per_package/STATUS_1.py"
'''

import cyte.food.USDA.examples as USDA_examples

import cyte.food.USDA.struct_2 as USDA_struct_2
import cyte.food.USDA.struct_2.ingredients.quantified_grove.printer as quantified_grove_printer
import cyte.food_and_supps.struct_2.ingredients.quantified_grove.mass_per_package as ingredient_mass_per_package

import json

from fractions import Fraction

def check_1 ():
	food_struct_2 = USDA_struct_2.calc (
		USDA_examples.retrieve ("branded/walnuts_1882785.json")
	)
	
	
	protein_mass_per_package = ingredient_mass_per_package.calc (
		food_struct_2,
		"protein"
	)
	assert (
		protein_mass_per_package == Fraction ("456528486851663599/7036874417766400")
	), protein_mass_per_package

	iron_mass_per_package = ingredient_mass_per_package.calc (
		food_struct_2,
		"iron"
	)
	assert (
		iron_mass_per_package == Fraction ("1461913475040736643/112589990684262400000")
	), iron_mass_per_package



	return;
	
	
checks = {
	'check 1': check_1
}
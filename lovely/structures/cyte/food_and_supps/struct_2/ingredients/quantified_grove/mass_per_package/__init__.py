
'''
import cyte.food_and_supps.struct_2.ingredients.quantified_grove.mass_per_package as ingredient_mass_per_package

ingredient = ingredient_mass_per_package.calc (
	treasure_struct_2,
	ingredient_name
)
'''

import cyte.food_and_supps.struct_2.ingredients.quantified_grove.mass_per_package as ingredient_mass_per_package
import cyte.food_and_supps.struct_2.ingredients.quantified_grove.find_ingredient as find_ingredient


from fractions import Fraction
import json

def calc (
	treasure_struct_2,
	ingredient_name = "",
	
	include_effectual = True,
	format = "fraction string grams"
):	
	ingredient = find_ingredient.calc (
		treasure_struct_2,
		ingredient_name
	)
	
	print ("found:", json.dumps (ingredient, indent = 4))
	
	try:
		mass_per_package = Fraction (ingredient ["mass"] ["per package"] [ format ])
	except Exception as E:
		mass_per_package = False
		print ("Exception:", E)
		
	if (type (mass_per_package) == Fraction):
		return mass_per_package
		
	try:
		mass_per_package = Fraction (ingredient ["effectual mass"] ["per package"] [ format ])
	except Exception as E:
		mass_per_package = False
		print ("Exception:", E)	
	
	if (type (mass_per_package) == Fraction):
		return mass_per_package


	raise Exception (f"Mass per package of ingredient { ingredient_name } could not be calculated.")

	
	
	
	
	
	
	
	
	
	
	
	
	
	
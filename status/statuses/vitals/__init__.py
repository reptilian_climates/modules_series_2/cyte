

def start ():
	def add_paths_to_system (paths):
		import pathlib
		from os.path import dirname, join, normpath
		import sys
		
		this_folder = pathlib.Path (__file__).parent.resolve ()	
		for path in paths:
			sys.path.insert (0, normpath (join (this_folder, path)))

	add_paths_to_system ([
		'../../structures_pip'
	])


	import pathlib
	from os.path import dirname, join, normpath

	this_folder = pathlib.Path (__file__).parent.resolve ()
	base_folder = normpath (join (this_folder, "../../.."))

	structures = normpath (join (base_folder, "lovely/structures"))
	structures_pip = normpath (join (base_folder, "lovely/structures_pip"))
	structure = normpath (join (structures, "cyte"))

	import sys
	if (len (sys.argv) >= 2):
		glob_string = structure + '/' + sys.argv [1]
		db_directory = False
	else:
		glob_string = structure + '/**/STATUS*.py'
		db_directory = normpath (join (this_folder, "db"))

	print ("glob:", glob_string)

	import body_scan
	scan = body_scan.start (
		# required
		glob_string = glob_string,
		simultaneous = True,
		
		# optional
		module_paths = [	
			structures,
			structures_pip
		],
		
		# optional
		relative_path = structure,
		
		db_directory = db_directory,
		
		records = 0
	)
	
	return scan;


def present (scan):
	import pathlib
	from os.path import dirname, join, normpath	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	
	print ()
	import json
	print ("scan returned alarms:", json.dumps (scan ["alarms"], indent = 4))
	print ("scan returned stats:", json.dumps (scan ["stats"], indent = 4))
		
	import body_scan.db as body_scan_db
	records = body_scan_db.records (
		db_directory = normpath (join (this_folder, f"db"))
	)
	records_tally = len (records)
	
	last_record = records [ records_tally - 1 ] ["stats"]
	prev_record = records [ records_tally - 2 ] ["stats"]
	
	difference = {
		"alarms": last_record ["alarms"] - prev_record ["alarms"],
		"empty": last_record ["empty"] - prev_record ["empty"],
		"checks": {
			"passes": last_record ["checks"]["passes"] - prev_record ["checks"]["passes"],
			"alarms": last_record ["checks"]["alarms"]  - prev_record ["checks"]["alarms"] ,
		}
	}
	

	import json
	print ("changes in records tallies (relevant if every check run):", json.dumps (difference, indent = 4))
		
	print ("records tally:", len (records))

	return;

	
	
if __name__ == "__main__":
	scan = start ()
	present (scan)
	
	
	

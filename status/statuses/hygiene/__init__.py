



def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures_pip'
])



def start ():
	import pathlib
	from os.path import dirname, join, normpath

	this_folder = pathlib.Path (__file__).parent.resolve ()
	base_folder = normpath (join (this_folder, "../../.."))

	structures = normpath (join (base_folder, "lovely/structures"))
	structures_pip = normpath (join (base_folder, "lovely/structures_pip"))

	checks_directory = normpath (join (this_folder, "checks"))
	db_directory = normpath (join (this_folder, "db"))


	import sys
	if (len (sys.argv) >= 2):
		glob_string = checks_directory + '/' + sys.argv [1]
	else:
		glob_string = checks_directory + '/**/hygiene_*.py'


	print ("glob:", glob_string)


	import body_scan
	scan = body_scan.start (
		# required
		glob_string = glob_string,
		simultaneous = True,
		
		# optional
		module_paths = [	
			structures,
			structures_pip
		],
		
		# optional
		relative_path = checks_directory,
		
		db_directory = db_directory,
		
		records = 0
	)

	print ()
	import json
	print ("hygiene scan returned alarms:", json.dumps (scan ["alarms"], indent = 4))
	print ("hygiene scan returned stats:", json.dumps (scan ["stats"], indent = 4))
	
	return scan
	
if __name__ == "__main__":
	start ()
	

#
#
#

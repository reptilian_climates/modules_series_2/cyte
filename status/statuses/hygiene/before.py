


def add_paths_to_system (paths):
	from os.path import dirname, join, normpath
	import pathlib
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures',
	'../../structures_pip'
])


import cyte.hygiene.__stem.start as start_stem
stem = start_stem.presently ()



def before_exit ():
	print ("at exit event called")
	stem.stop ()

import atexit
import signal
atexit.register (before_exit)
signal.signal (signal.SIGTERM, before_exit)
signal.signal (signal.SIGINT, before_exit)

import time
time.sleep (1)
print ()
print ("waiting for an exit event")
stem.process.wait ()
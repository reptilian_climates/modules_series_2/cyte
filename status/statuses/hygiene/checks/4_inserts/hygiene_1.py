

'''
	python3 status_hygiene.py '4_inserts/**/*.py'
'''

ports = [ 31498, 31499, 31500 ]

import botanical.cycle as cycle

import cyte.hygiene._system.start as hygiene_start
import cyte.hygiene._system.climate as climate
import cyte.hygiene._system.connect as connect
import cyte.hygiene._system.cannot_connect as cannot_connect

import cyte.hygiene._node.architecture as hygiene_architecture

import time
import atexit
import pathlib
import shutil


def before_exit ():
	print ("at exit event called")

import atexit
atexit.register (before_exit)

'''
import signal
signal.signal (signal.SIGTERM, before_exit)
signal.signal (signal.SIGINT, before_exit)
'''


def erase_dir_if_can (directory):
	try:
		shutil.rmtree (directory)
	except Exception as E:
		print ("shutil exception:", E)

def check_1 ():
	node_directory = str (pathlib.Path (__file__).parent.resolve ()) + "/rethinkdb_data"
	erase_dir_if_can (node_directory)

	climate.change ("ports", {
		"driver": ports [0],
		"cluster": ports [1],
		"http": ports [2]
	})
	
	ly = hygiene_start.now (
		process = {
			"cwd": pathlib.Path (__file__).parent.resolve ()	
		}
	)
	
	[ r, c ] = connect.now ()

	db_list = r.db_list ().run (c)
	print ("db list:", db_list)
	assert (db_list == ['rethinkdb', 'test'])
	
	hygiene_architecture.create ()
	
	
	
	
	
	
	
	'''
	
	'''	
	ly.stop ()
	time.sleep (1)

	cannot_connect.ensure ()

	
	shutil.rmtree (node_directory)

	return;
	
	
checks = {
	"can build the architecture": check_1
}




#
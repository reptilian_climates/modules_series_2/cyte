

'''
	python3 status_hygiene.py '3_architecture/**/*.py'
'''

ports = [ 31495, 31496, 31497 ]

import botanical.cycle as cycle

import cyte.hygiene._system.start as hygiene_start
import cyte.hygiene._system.climate as climate
import cyte.hygiene._system.connect as connect
import cyte.hygiene._system.cannot_connect as cannot_connect

import cyte.hygiene._node.architecture as hygiene_architecture

import time
import atexit
import pathlib
import shutil

def check_1 ():
	def before_exit ():
		print ('before exiting..')

	atexit.register (before_exit)

	node_directory = str (pathlib.Path (__file__).parent.resolve ()) + "/rethinkdb_data"
	try:
		shutil.rmtree (node_directory)
	except Exception as E:
		print ("shutil exception:", E)

	climate.change ("ports", {
		"driver": ports [0],
		"cluster": ports [1],
		"http": ports [2]
	})
	
	cannot_connect.ensure ()
	
	ly = hygiene_start.now (
		process = {
			"cwd": pathlib.Path (__file__).parent.resolve ()	
		}
	)
	
	[ r, c ] = connect.now ()

	db_list = r.db_list ().run (c)
	print ("db list:", db_list)
	assert (db_list == ['rethinkdb', 'test'])

	
	hygiene_architecture.create ()
	'''
	
	'''	
	ly.stop ()
	time.sleep (1)

	cannot_connect.ensure ()

	
	shutil.rmtree (node_directory)

	return;
	
	
checks = {
	"can build the architecture": check_1
}




#
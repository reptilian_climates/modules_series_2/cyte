

'''
	python3 status/statuses/hygiene/__init__.py '2_start_and_stop/**/*.py'
'''

ports = [ 31492, 31493, 31494 ]

import botanical.cycle as cycle

import cyte.hygiene._system.start as hygiene_start
import cyte.hygiene._system.climate as climate
import cyte.hygiene._system.connect as connect
import cyte.hygiene._system.cannot_connect as cannot_connect

import time
import atexit
import pathlib
import shutil

def check_1 ():
	def before_exit ():
		print ('before exiting..')

	atexit.register (before_exit)

	climate.change ("ports", {
		"driver": ports [0],
		"cluster": ports [1],
		"http": ports [2]
	})
	
	
	ly = hygiene_start.now (
		process = {
			"cwd": pathlib.Path (__file__).parent.resolve ()	
		}
	)
	
	[ r, c ] = connect.now ()

	db_list = r.db_list ().run (c)
	print ("db list:", db_list)
	assert (db_list == ['rethinkdb', 'test'])

	ly.stop ()
	
	time.sleep (1)

	cannot_connect.ensure (loops = 2)

	node_directory = str (pathlib.Path (__file__).parent.resolve ()) + "/rethinkdb_data"
	shutil.rmtree (node_directory)

	return;
	
	
checks = {
	"can start and stop": check_1
}




#
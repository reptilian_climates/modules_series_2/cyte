

'''
	python3 status_hygiene.py '1_no_connection/**/*.py'
'''

import botanical.cycle as cycle

import cyte.hygiene._system.start as hygiene_start
import cyte.hygiene._system.climate as climate
import cyte.hygiene._system.connect as connect
import cyte.hygiene._system.cannot_connect as cannot_connect

import time
import atexit

def check_1 ():
	def before_exit ():
		print ('before exiting..')

	atexit.register (before_exit)

	climate.change ("ports", {
		"driver": 31489,
		"cluster": 31490,
		"http": 31491	
	})

	#ly = hygiene_start.now ()
	#ly.stop ()
	
	cannot_connect.ensure ()
		

	return;
	
	
checks = {
	"exception raised if can't connect": check_1
}




#
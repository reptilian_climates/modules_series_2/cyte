
'''
	python3 status_api.py
'''

import sys
print (sys.argv)


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../../structures_pip'
])


import pathlib
from os.path import dirname, join, normpath

this_directory = pathlib.Path (__file__).parent.resolve ()
base_directory = normpath (join (this_directory, "../../.."))

structures = normpath (join (base_directory, "lovely/structures"))
structures_pip = normpath (join (base_directory, "lovely/structures_pip"))

checks = normpath (join (structures, "cyte"))

if (len (sys.argv) >= 2):
	glob_string = checks + '/' + sys.argv [1]
	db_directory = False
else:
	glob_string = checks + '/**/API_STATUS*.py'
	db_directory = normpath (join (this_directory, "db"))

import body_scan
scan = body_scan.start (
	glob_string = glob_string,
	relative_path = checks,
	module_paths = [	
		structures,
		structures_pip
	],
	
	simultaneous = True,
	
	db_directory = db_directory
)

print ()
import json
print ("scan returned alarms:", json.dumps (scan ["alarms"], indent = 4))
print ("scan returned stats:", json.dumps (scan ["stats"], indent = 4))


#
#
#

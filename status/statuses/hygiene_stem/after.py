

import psutil
import pathlib
from os.path import dirname, join, normpath

this_folder = pathlib.Path (__file__).parent.resolve ()
node_pid = normpath (join (this_folder, "node_pid.txt"))

import cyte.hygiene.__stem.climate as stem_climate
import cyte.hygiene._system.connect as connect
	
def check_1 ():
	print ()
	print ()
	print ("starting the after routine")
	print ()
	print ()

	stem_climate.change ()
	[ r, c ] = connect.now ()

	fp = open (node_pid)
	pid = fp.read ()
	fp.close ()

	p = psutil.Process (int (pid))
	p.terminate ()
	
	import cyte.hygiene._system.cannot_connect as cannot_connect
	cannot_connect.ensure (
		loops = 3
	)

	

	return;
	
checks = {
	"check 1": check_1
}
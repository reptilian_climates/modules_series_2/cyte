


import pathlib
from os.path import dirname, join, normpath

this_folder = pathlib.Path (__file__).parent.resolve ()
node_pid = normpath (join (this_folder, "node_pid.txt"))


def check_1 ():
	try:
		import os
		os.remove (node_pid)
	except Exception:
		pass;

	import cyte.hygiene.__stem.start as start_stem
	stem = start_stem.presently (
		rethink_params = [
			f"--pid-file '{ node_pid }'",
			"--daemon"
		]
	)


	return;
	
checks = {
	"check 1": check_1
}
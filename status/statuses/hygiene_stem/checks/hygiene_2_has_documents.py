



'''
	python3 status_hygiene.py 'hygiene_2.py'
'''



import time
import atexit

import cyte.hygiene.__stem.climate as stem_climate
import cyte.hygiene._system.connect as connect
import cyte.hygiene.foods.inventory.doc.list.fdc_ids as fdc_id_list

import cyte.hygiene._system.climate as climate



def check_1 ():
	stem_climate.change ()
	ports = climate.find ("ports")
	print ("ports:", ports)

	[ r, c ] = connect.now ()

	fdc_ids = fdc_id_list.find ()
	print (fdc_ids)
	
	assert ("1960255" in fdc_ids)

	return;
	
	
checks = {
	"has documents": check_1
}




#
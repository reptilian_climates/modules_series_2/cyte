
'''
	pip install --requirement STRUCTURES_PIP.UTF8 --target STRUCTURES_PIP --upgrade
'''

'''
	python3 status.py
	
	python3 status/statuses/api/__init__.py 
	python3 status/statuses/vitals/__init__.py 
	
	python3 status/statuses/hygiene/__init__.py 
	python3 status/statuses/hygiene_stem/__init__.py 
'''


def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'structures_pip'
])

import statuses.vitals as vitals_status
import statuses.hygiene as hygiene_status
#import statuses.hygiene_stem as hygiene_stem_status

print ('concurrency is broken')
exit ()

import botanical.flow.demux_mux as demux_mux

def circuit_1 ():
	def circuit ():
		return vitals_status.start ()

	return circuit
	
def circuit_2 ():
	def circuit ():
		return hygiene_status.start ()

	return circuit

proceeds_statement = demux_mux.start ([
	circuit_1 (),
	circuit_2 ()
])


vitals_scan = proceeds_statement [0]
hygiene_scan = proceeds_statement [1]

alarms = [
	* vitals_scan ["alarms"],
	* hygiene_scan ["alarms"]
]

stats = [
	* vitals_scan ["stats"],
	* hygiene_scan ["stats"]
]

import json
print (json.dumps (alarms, indent = 4))
print (json.dumps (stats, indent = 4))
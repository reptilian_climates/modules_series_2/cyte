
'''
	This needs to be run from this CWD!
'''

'''
git add --all
git commit -m "com"
git push --set-upstream git@gitlab.com:reptilian_climates/modules_series_2/cyte.git treasury
'''

'''
	cp module.r.html module.txt
	(rm -rf dist && python3 -m build --sdist && twine upload dist/*)
'''

'''
	relevant:
		https://github.com/achillesrasquinha/pipupgrade
'''

import pathlib
from os.path import dirname, join, normpath
import sys

this_folder = pathlib.Path (__file__).parent.resolve ()	
dist = normpath (join (this_folder, "dist"))

module_r_html = normpath (join (this_folder, "module.r.html"))
module_txt = normpath (join (this_folder, "module.txt"))


import os
os.system (f"cp '{ module_r_html }' '{ module_txt }'")
os.system (f"rm -rf '{ dist }'")
os.system (f"python3 -m build --sdist")
os.system (f"twine upload dist/*")

